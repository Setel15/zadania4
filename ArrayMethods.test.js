const { mapFn, filterFn, reduceFn, reduceRightFn, everyFn, someFn, entriesFn } = require("./ArrayMethods")
const numberArray = [5, 8, 3, 28, 10]
const actionCallback = (elem) => elem ** 2
const booleanCallback = (elem) => elem > 10
const reduceCallback = (acc, elem) => acc + elem
describe("testing if custom functions returns equal values to Javascript functions", () => {
    test("map function", () => {
        expect(mapFn(numberArray, actionCallback)).toStrictEqual(numberArray.map(actionCallback))
    })
    test("filter function", () => {
        expect(filterFn(numberArray, booleanCallback)).toStrictEqual(numberArray.filter(booleanCallback))
    })
    test("reduce function", () => {
        expect(reduceFn(numberArray, reduceCallback, 0)).toStrictEqual(numberArray.reduce(reduceCallback))
    })
    test("reduceRight function", () => {
        expect(reduceRightFn(numberArray, reduceCallback, 0)).toStrictEqual(numberArray.reduceRight(reduceCallback))
    })
    test("every function", () => {
        expect(everyFn(numberArray, booleanCallback)).toStrictEqual(numberArray.every(booleanCallback))
    })
    test("some function", () => {
        expect(someFn(numberArray, booleanCallback)).toStrictEqual(numberArray.some(booleanCallback))
    })
    test("entries function", () => {
        let myIterator = entriesFn(numberArray)
        let jsIterator = numberArray.entries()
        expect(myIterator.next().value).toStrictEqual(jsIterator.next().value)
    })
})