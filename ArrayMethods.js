module.exports.mapFn = (array, callback) => {
    let newArray = []
    for (elem of array) {
        newArray.push(callback(elem))
    }
    return newArray
}
module.exports.filterFn = (array, callback) => {
    let newArray = []
    for (elem of array) {
        if (callback(elem))
            newArray.push(elem)
    }
    return newArray
}
module.exports.reduceFn = (array, callback, inital = 0) => {
    for (elem of array) {
        inital = callback(inital, elem)
    }
    return inital
}
module.exports.reduceRightFn = (array, callback, initial = 0) => {
    for (elem of array.reverse()) {
        inital = callback(inital, elem)
    }
    return inital
}
module.exports.everyFn = (array, callback) => {
    for (elem of array) {
        if (!callback(elem))
            return false
    }
    return true
}
module.exports.someFn = (array, callback) => {
    for (elem of array) {
        if (callback(elem))
            return true
    }
    return false
}
module.exports.entriesFn = (array) => {
    return {
        value: undefined,
        next: function () {
            this.index++
            return {
                value: this.index < array.length ? [this.index, array[this.index]] : undefined,
                next: this.next,
                index: this.index,
            }
        },
        index: -1,
    }
}
const entriesFn = (array) => {
    return {
        value: undefined,
        next: function () {
            this.index++
            return {
                value: this.index < array.length ? [this.index, array[this.index]] : undefined,
                next: this.next,
                index: this.index,
            }
        },
        index: -1,
    }
}
let iterator = entriesFn([1, 2, 3, 4, 5])
let iterator1 = [1, 2, 3, 4, 5].entries()
console.log(iterator.next().value)
console.log(iterator1.next().value)